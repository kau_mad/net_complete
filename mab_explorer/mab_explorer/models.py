import kdtree
from sklearn.exceptions import NotFittedError
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import StandardScaler
import numpy as np


class LearningModel(object):

    def fit(self, X, Y):
        pass

    def predict(self, x):
        pass


class IncrementalModel(LearningModel):

    def __init__(self):
        self._fitted = False
        self.iter = 0

    def partial_fit(self, x, y):
        pass

    def check_fitted(self):
        if not self._fitted:
            raise NotFittedError("The model is not trained with data")


class Item(object):
    """
    To be used in the KD-tree
    The field "data" contains a payload
    """
    def __init__(self, coords, value):
        self.coords = coords
        self.value = value

    def __len__(self):
        return len(self.coords)

    def __getitem__(self, i):
        return self.coords[i]

    def __repr__(self):
        return 'Item({}, {})'.format(self.coords.__str__(), self.value)


class KNearestNeighborModel(IncrementalModel):

    def __init__(self, dim, k=10):
        self._dim = dim
        self.tree = kdtree.create(dimensions=dim)
        self._fitted = False
        self._k = k
        super().__init__()

    def fit(self, X, Y):
        self.tree = kdtree.create(dimensions=self._dim)
        for x, y in zip(X, Y):
            self.tree.add(Item(x, y))
        self._fitted = True
        self.iter += 1

    def partial_fit(self, x, y):
        self.check_fitted()
        self.tree.add(Item(x, y))
        self.iter += 1

    def predict(self, x, k=0):
        """
        returns the sum of data value weighted by the inverse distance
        to the point x.
        """
        # TODO check if x is an array
        prediction = sum([item.data.value/(d+0.01) for item, d in
                          self.tree.search_knn(x, k=k if k > 0 else self._k)])
        return prediction

    def get_k_nearest_points(self, x, k=5):
        """
        returns coordinates of the k-nearest neighbors of point x.
        """
        if x.ndim > 1:
            x = x.reshape(-1)
        return [item.data.coords for item, d in self.tree.search_knn(x, k)]

    def get_k_nearest_dists(self, x, k=5):
        """
        returns the distances of the k nearest neighbors of point x.
        """
        if x.ndim > 1:
            x = x.reshape(-1)
        return [d for _, d in self.tree.search_knn(x, k)]

    def remove_item(self, x):
        if x.ndim > 1:
            x = x.reshape(-1)
        self.tree.remove(Item(x, '_'))

    @property
    def fitted(self):
        return self._fitted


class ScaledKNearestNeighborModel(IncrementalModel):

    def __init__(self, dim, refit_m=0):
        self._scaler = StandardScaler()
        self._knn_model = KNearestNeighborModel(dim)
        self._iter = 0
        self._X = []
        self._Y = []
        self._refit_m = refit_m
        super().__init__()

    def fit(self, X, Y):
        self._knn_model.fit(self._scaler.fit_transform(X), Y)
        self._X = X
        self._Y = Y

    def partial_fit(self, x, y):
        # self._X = np.vstack([self._X, x])
        # self._Y = np.vstack([self._Y, y])
        self._X.append(x.reshape(-1))
        self._Y.append(y)

        if self._refit_m > 0 and self._knn_model.iter % self._refit_m == 0:
            self.fit(self._X, self._Y)
            print(np.sum(self._Y))

        else:
            self._knn_model.partial_fit(self._scaler.transform(x).reshape(-1), y)

    def predict(self, x):
        if x.ndim == 1:
            x = x.reshape(1, -1)
        return self._knn_model.predict(self._scaler.transform(x).reshape(-1))

    def get_k_nearest_points(self, x, k=5):
        if x.ndim == 1:
            x = x.reshape(1, -1)
        return self._knn_model.get_k_nearest_points(self._scaler.transform(x)
                                                    .reshape(-1), k)

    def get_k_nearest_dists(self, x, k=5):
        if x.ndim == 1:
            x = x.reshape(1, -1)
        return self._knn_model.get_k_nearest_dists(self._scaler.transform(x)
                                                   .reshape(-1), k)

    def remove_point(self, x):
        if x.ndim == 1:
            x = x.reshape(1, -1)
        self.knn_model.remove_item(self._scaler.transform(x))

    @property
    def knn_model(self):
        return self._knn_model

    @property
    def fitted(self):
        return self.knn_model.fitted


class NormalizedKNeighborsRegressor(LearningModel):

    def __init__(self, n_neighbors=5, weights=None, n_jobs=-2):
        self._scaler = StandardScaler()
        self._knn_model = KNeighborsRegressor(n_neighbors=n_neighbors,
                                              weights=weights, n_jobs=n_jobs)

    def fit(self, X, Y):
        X = self._scaler.fit_transform(X)
        self._knn_model.fit(X, Y)

    def predict(self, x):
        x = self._scaler.transform(x)
        return self._knn_model.predict(x)

    def kneighbors(self, x, n_neighbors=5):
        x = self._scaler.transform(x)
        return self._knn_model.kneighbors(x, n_neighbors=n_neighbors)

    @property
    def n_neighbors(self):
        return self._knn_model.n_neighbors

    def __str__(self):
        return 'KNN(n=%d)'%self._knn_model.n_neighbors
