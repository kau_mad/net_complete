# -*- coding: utf-8 -*-
import numpy as np
import networkx as nx
from bandits.bandit import MultiArmedBandit
from bandits.utils import Response, BanditExecuter
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from collections import defaultdict
import random


class NetworkExploringBandit(MultiArmedBandit):
    """
    This bandit models exploration of an incomplete network.
     Here each arm corresponds to a group of observable nodes in the network
    """

    def __init__(self, graph_sample, graph_oracle, k, n, node_policy='random',
                 epsilon=0., alpha=0., degree_threshold=-1):
        self.graph_sample = graph_sample
        self._original_graph = graph_sample.copy()
        self._graph_oracle = graph_oracle
        self.k = k
        self.n = n
        self.optimal = 0
        self.epsilon = epsilon
        self.alpha = alpha
        self.num_features = 4  # TODO remove this
        self._A = {}
        self._invA = {}
        self._b = {}
        self._theta = {}
        self._model = {}
        self.node_policy = node_policy
        self._init_env()
        self.t = 0
        self.degree_threshold = degree_threshold

    def __str__(self):
        return 'k={0} {1}'.format(self.k, self.node_policy)

    def reset(self):
        self.action_values = np.zeros(self.k)
        self.graph_sample = self._original_graph.copy()
        self.optimal = 0
        self.t = 0
        self._init_env()

    def pull(self, action):
        """        
        :param action: this indicates the cluster id of any node which should be
         probed. A node is randomly sampled from this cluster and probed. 
        :return: reward and a boolean value indicating whether the action is 
        the optimal. 
        """
        selected_node = -1
        if self.node_policy == 'random':
            selected_node = random.choice(self._arms[action])
        elif self.node_policy == 'max':
            arm_degrees = dict(self.graph_sample.degree(self._arms[action]))
            selected_node = max(arm_degrees.keys(), key=lambda k: arm_degrees[k])
        elif self.node_policy == 'linear':
            max_score = -1 * np.inf
            selected_node = -1
            rand_val = random.random()

            if self.epsilon > 0 and rand_val < self.epsilon:
                selected_node = self._arms[action][random.randint(0, len(self._arms[action])-1)]
            else:
                rlogt = self.alpha*np.sqrt(self.num_features)*np.log(self.t+1.)
                for node in self._arms[action]:
                    context = self._get_node_features(node)
                    score = float(self._theta[action].T.dot(context))
                    score -= self.graph_sample.degree(node)
                    if self.alpha > 0:
                        score += (rlogt*(context.T.dot(self._invA[action])).dot(context))[0][0]
                    if score >= max_score:
                        max_score = score
                        selected_node = node
        elif self.node_policy == 'knn':
            # TODO mention this threshold as a param
            if len(self._contexts[action]) > 20:
                score = [(self._model[action].predict(self._get_node_features(node).reshape(1, -1)) -
                                                      self.graph_sample.degree(node))
                         for node in self._arms[action]]
                selected_node = self._arms[action][np.argmax(score)]
            else:
                selected_node = random.choice(self._arms[action])

        neighbors = self._graph_oracle.get_neighbors(selected_node)
        unvisited_neighbors = set(neighbors) - set(self._visited_nodes)
        new_nodes = set(neighbors) - set(self.graph_sample.nodes())
        # triangles0 = nx.triangles(self.graph_sample, selected_node)
        selected_context = self._get_node_features(selected_node)
        self._contexts[action].append(selected_context.reshape(-1))

        for neighbor in unvisited_neighbors:
            self.graph_sample.add_edge(selected_node, neighbor)
            cluster_id = self._kmeans.predict(self._get_node_features(neighbor).reshape(1, -1))[0]
            # cluster_id = random.choice(range(self._kmeans.n_clusters))
            if neighbor in new_nodes:
                self._observed_nodes.add(neighbor)
            else:
                self._arms[self._node_membership[neighbor]].remove(neighbor)
            self._node_membership[neighbor] = cluster_id
            self._arms[cluster_id].append(neighbor)

        self._visited_nodes.append(selected_node)
        self._arms[action].remove(selected_node)

        active_arms = [len(self._arms[action]) != 0 for action in range(self.k)]
        # TODO recalculation of cluster centers
        # for cluster_id in range(self._kmeans.n_clusters):
        #     if len(self._arms[cluster_id]) > 0:
        #         node_features = np.array([self._get_node_features(node).flatten() for node in self._arms[cluster_id]])
        #         self._kmeans.cluster_centers_[cluster_id] = np.mean(node_features, axis=0)
        reward = len(new_nodes)
        #reward = nx.triangles(self.graph_sample, selected_node)-triangles0
        #reward = len(self.graph_sample.edges(selected_node)) - edges0
        # print("action= %d \t reward=%d"%(action, reward))
        y = len(neighbors)
        self._rewards[action].append(len(neighbors))
        self._update_model(action, y, features=selected_context)
        return Response(reward, action == self.optimal, active_arms)

    def _init_env(self):
        self._visited_nodes = []
        self._observed_nodes = set(self.graph_sample.nodes())
        # self._observable_nodes = self.graph_sample.nodes()
        # to keep track of which cluster an arm belongs to
        self._arms = defaultdict(list)
        self._contexts = defaultdict(list)
        self._rewards = defaultdict(list)
        self._node_features = {}

        self._degrees = self.graph_sample.degree()
        X = np.zeros(shape=[self.graph_sample.number_of_nodes(), self.num_features], dtype=float)

        # TODO implement this using transformer API
        for i, node in enumerate(self.graph_sample.nodes()):
            X[i, :] = self._get_node_features(node).flatten()
        self._kmeans = KMeans(n_clusters=self.k, random_state=0).fit(np.array(X))
        self._node_membership = dict(zip(self.graph_sample.nodes(), self._kmeans.labels_))
        for node, label in self._node_membership.items():
            self._arms[label].append(node)

        # initializing linear regression model
        for i in range(self.k):
            if self.node_policy == 'linear':
                self._A[i] = np.identity(self.num_features, dtype=float)
                self._invA[i] = np.identity(self.num_features, dtype=float)
                self._b[i] = np.zeros((self.num_features, 1), dtype=float)
                self._theta[i] = np.random.rand(self.num_features, 1)
            elif self.node_policy == 'knn':
                knn_flow = [('scaler', StandardScaler(with_mean=True)),
                            ('knn', KNeighborsRegressor(n_neighbors=5,  weights='distance'))]
                self._model[i] = Pipeline(knn_flow)

    def get_current_state(self):
        return self.graph_sample

    def _get_node_features(self, node):
        if node in self._node_features.keys():
            return self._node_features[node]

        neighbors = list(self.graph_sample.neighbors(node))
        neigh_deg = [d for n, d in self.graph_sample.degree(neighbors)]
        neigh_visited_deg = [sum(1 for n2 in self.graph_sample.neighbors(n) if n2 in self._visited_nodes)
                             for n, d in self.graph_sample.degree(neighbors)]
        visited_ratio = [a*10./b for (a, b) in zip(neigh_visited_deg, neigh_deg)]
        # mean_neigh_deg = np.mean(neigh_deg)
        med_neigh_deg = np.median(neigh_deg)
        max_neigh_deg = np.max(neigh_deg)
        num_triangles = nx.triangles(self.graph_sample, nodes=node)
        self._node_features[node] = np.array([self.graph_sample.degree(node), med_neigh_deg,
                         max_neigh_deg, np.mean(visited_ratio)]).reshape(self.num_features, 1)

        return self._node_features[node]

    def _update_model(self, action, reward, features, alpha=1.):
        """
        applies OLS iteratively
        :param action:
        :param reward:
        :param features:
        :param alpha:
        :return:
        """
        if self.node_policy == 'linear':
            self._A[action] = (2.-alpha) * self._A[action] + alpha * features.dot(features.T)
            self._b[action] = (2.-alpha) * self._b[action] + alpha * reward * features
            self._invA[action] = np.linalg.inv(self._A[action])
            self._theta[action] = self._invA[action].dot(self._b[action])
        elif self.node_policy == 'knn':
            if len(self._contexts[action])>10:
                self._model[action].fit(self._contexts[action], self._rewards[action])
        self.t += 1

    def get_cluster_centers(self):
        return self._kmeans.cluster_centers_

    @property
    def rewards(self):
        return self._rewards


class NetworkExploringExecuter(BanditExecuter):
    def __init__(self, graph_sample, graph_oracle, num_features=4, degree_threshold=0):
        self.graph_sample = graph_sample
        self._original_graph = graph_sample.copy()
        self._graph_oracle = graph_oracle
        self._visited_nodes = []
        self.num_features = num_features
        self._node_features = {}
        self.degree_threshold = degree_threshold

    def reset(self):
        self.graph_sample = self._original_graph.copy()
        self._visited_nodes = []
        self._node_features = {}

    def get_features(self):
        raise NotImplementedError

    def get_context(self, node, recalc=False):
        """
        returns the feature vector of a node.
        Following features are computed based on the visible network
        1. observed degree
        2. median of the degrees of neighbors
        3. maximum degree in the neighborhood
        4. number of triangles the node participates in
        :param recalc: if this parameter is set to true, features are
        calculated for the node. Otherwise, features are obtained from
        the cache
        :param node:
        :return:
        """

        if not recalc and node in self._node_features.keys():
            return self._node_features[node]

        neighbors = list(self.graph_sample.neighbors(node))
        neigh_deg = [d for n, d in self.graph_sample.degree(neighbors)]
        neigh_visited_deg = [len(set(self.graph_sample.neighbors(n)).intersection(set(self._visited_nodes)))
                             for n in neighbors]
        visited_ratio = np.sum(neigh_visited_deg)/np.sum(neigh_deg)
        # mean_neigh_deg = np.mean(neigh_deg)
        med_neigh_deg = np.median(neigh_deg)
        max_neigh_deg = np.max(neigh_deg)
        # num_triangles = nx.triangles(self.graph_sample, nodes=node)
        self._node_features[node] = np.array([self.graph_sample.degree(node), med_neigh_deg,
                         max_neigh_deg, visited_ratio]).reshape(self.num_features, 1)

        return self._node_features[node]

    def get_bias(self, node):
        return self.graph_sample.degree(node)

    def run(self, action):
        """
        this function is called inside "pull" function of a bandit

        :param action: the node to be probed
        :return: degree of the node
        """
        nodes1 = self.graph_sample.number_of_nodes()
        neighbors = self._graph_oracle.get_neighbors(action)
        unvisited_neighbors = set(neighbors) - set(self._visited_nodes)
        self._visited_nodes.append(action)
        for node in unvisited_neighbors:
            self.graph_sample.add_edge(action, node)
        for node in unvisited_neighbors:
            self.get_context(node, recalc=True)
        if self.degree_threshold > 0:
            score = 1 if len(neighbors) >= self.degree_threshold else 0
        else:
            nodes2 = self.graph_sample.number_of_nodes()
            score = nodes2 - nodes1
        reward = len(neighbors)
        return reward, score

    def get_current_state(self):
        return self.graph_sample

    @property
    def arms(self):
        unvisited_nodes = set(self.graph_sample.nodes) - set(self._visited_nodes)
        return list(unvisited_nodes)
