# -*- coding: utf-8 -*-
import argparse
import random
from itertools import cycle
import os
import pickle

import networkx as nx
import numpy as np
from sklearn.gaussian_process.kernels import RBF, ConstantKernel, \
    WhiteKernel, Matern
from bandits import Agent, Environment, UCBPolicy
import matplotlib.pyplot as plt
from collections import deque, defaultdict
import seaborn as sns
from bandits.agent import BayesianLinearAgent, Exp3Agent, MetricAgent, \
    LinRelAgent
from bandits.bandit import MetricBandit
from bandits.policy import Exp3Policy, GpUcbPolicy, LinUCBPolicy, \
    EpsilonGreedyPolicy, GreedyPolicy, KNNUCBPolicy

from mab_explorer import GraphOracle
from mab_explorer import NormalizedKNeighborsRegressor
from mab_explorer import NetworkExploringExecuter


class GraphSample(object):
    def __init__(self, num_nodes, num_edges, optimal=None, scores=None, label=None):
        self.scores = scores
        self.num_nodes = num_nodes
        self.num_edges = num_edges
        self.optimal = optimal
        self.label = label
        self.rewards = None


class BanditConfig(object):
    def __init__(self, bandit, agent, num_experiments=1, node_policy=None):
        self.bandit = bandit
        self.agent = agent
        self.node_policy = node_policy
        self.num_experiments = num_experiments

    def __str__(self):
        return self.agent.__str__() + '-' + self.agent.policy.__str__()


def mod_sample(graph_oracle, graph_sample, budget, policy='mod', rand=0.1,
               degree_threshold=0):
    """
    
    :param graph_oracle: 
    :param graph_sample: 
    :param budget: 
    :param policy: 
    :return: 
    """
    num_probes = 0
    visited_nodes = set()
    # num_neighbors_added = []
    observed_nodes = set(graph_sample.nodes())
    scores = np.zeros(budget)
    to_visit = deque(observed_nodes)

    while num_probes < budget:
        observable_nodes = set(graph_sample.nodes()) - visited_nodes

        if policy == 'mod':
            observed_degree = dict(graph_sample.degree(observable_nodes))
            mod_node = list(observed_degree.keys())[np.argmax(list(observed_degree.values()))]
        elif policy == 'random':
            mod_node = list(observable_nodes)[random.choice(range(len(observable_nodes)))]
        elif policy == 'oracle':
            oracle_degree = dict(graph_oracle.degree(observable_nodes))
            observed_degree = dict(graph_sample.degree(observable_nodes))
            excess_degree = np.subtract(list(oracle_degree.values()), list(observed_degree.values()))
            mod_node = list(observed_degree.keys())[np.argmax(excess_degree)]
        elif policy == 'bfs':
            mod_node = to_visit.popleft()
        elif policy == 'mod_random':
            if random.random() > rand:
                observed_degree = dict(graph_sample.degree(observable_nodes))
                mod_node = list(observed_degree.keys())[np.argmax(list(observed_degree.values()))]
            else:
                mod_node = list(observable_nodes)[random.choice(range(len(observable_nodes)))]
        mod_neighbors = graph_oracle.get_neighbors(mod_node)
        new_nodes = set(mod_neighbors) - observed_nodes

        if policy == 'bfs':
            to_visit.extend(new_nodes)

        # triangles0 = nx.triangles(graph_sample, mod_node)
        edges0 = len(graph_sample.edges(mod_node))
        for neighbor in mod_neighbors:
            graph_sample.add_edge(mod_node, neighbor)
            observed_nodes.add(neighbor)

        if degree_threshold > 0:
            scores[num_probes] = 1 if len(mod_neighbors) >= degree_threshold else 0
        else:
            scores[num_probes] = len(new_nodes)
        # scores[num_probes] = nx.triangles(graph_sample, mod_node) - triangles0
        # scores[num_probes] = len(graph_sample.edges(mod_node)) - edges0
        num_probes += 1
        # num_neighbors_added.append(len(new_nodes))
        visited_nodes.add(mod_node)

    return graph_sample, scores


def run_experiments(graph_oracle, graph_sample, budget, sampling_func, policy,
                    experiments=1, plot=True, degree_threshold=0):
    graph_nodes = 0.
    graph_edges = 0.
    avg_scores = np.zeros(budget)

    for _ in range(experiments):
        output_graph, scores = sampling_func(graph_oracle, graph_sample.copy(), budget=budget,
                                             policy=policy, degree_threshold=degree_threshold)
        graph_nodes += output_graph.number_of_nodes()
        graph_edges += output_graph.number_of_edges()
        print("nodes: %d, edges: %d" % (output_graph.number_of_nodes(), output_graph.number_of_edges()))
        avg_scores += scores
        # TODO observe degree distribution of the output graph

    if plot:
        plt.plot(avg_scores / experiments)
        plt.title("Maximum Observed degree")
        plt.show()
    return GraphSample(graph_nodes / experiments, graph_edges / experiments, optimal=None,
                       scores=avg_scores / experiments, label=policy)


def run_bayesian_bandit(graph_oracle, graph_sample, budget,
                        num_experiments=1, plot=True):
    num_features = 4
    bandit_exec = NetworkExploringExecuter(graph_sample, graph_oracle)
    bandit = MetricBandit(num_features, bandit_exec)
    agent = BayesianLinearAgent(bandit, epsilon=1. / budget, has_bias=True)

    env = Environment(bandit, agent, 'Network exploring bandit')
    avg_scores = 0.
    avg_optimal = 0.
    graph_nodes = 0.
    graph_edges = 0.
    print("Bayesian Linear bandit is running")

    for _ in range(num_experiments):
        scores, optimal = env.run(trials=budget)
        avg_scores += scores
        avg_optimal += optimal
        graph_nodes += bandit_exec.graph_sample.number_of_nodes()
        graph_edges += bandit_exec.graph_sample.number_of_edges()
        print("nodes: %d, edges: %d" % (bandit_exec.graph_sample.number_of_nodes(),
                                        bandit_exec.graph_sample.number_of_edges()))

    avg_scores /= num_experiments
    avg_optimal /= num_experiments

    if plot:
        env.plot_results(avg_scores, avg_optimal)

    out = GraphSample(graph_nodes / num_experiments,
                      graph_edges / num_experiments, optimal=avg_optimal,
                      scores=avg_scores, label=agent.__str__())
    # out.rewards = bandit.rewards
    return out


def bandit_sample(bandit_config, budget, plot=True):
    bandit = bandit_config.bandit
    env = Environment(bandit, bandit_config.agent, 'Network exploring bandit')
    avg_scores = 0.
    avg_optimal = 0.
    graph_nodes = 0.
    graph_edges = 0.
    print("%s bandit is running" % bandit_config.__str__())

    for _ in range(bandit_config.num_experiments):
        scores, optimal = env.run(trials=budget)
        avg_scores += scores
        avg_optimal += optimal
        final_graph = bandit.get_current_state()
        graph_nodes += final_graph.number_of_nodes()
        graph_edges += final_graph.number_of_edges()
        print("nodes: %d, edges: %d" % (final_graph.number_of_nodes(),
                                        final_graph.number_of_edges()))

    avg_scores /= bandit_config.num_experiments
    avg_optimal /= bandit_config.num_experiments

    if plot:
        env.plot_results(avg_scores, avg_optimal)

    out = GraphSample(graph_nodes / bandit_config.num_experiments,
                      graph_edges / bandit_config.num_experiments, optimal=avg_optimal,
                      scores=avg_scores, label=bandit_config.__str__())
    # out.rewards = bandit.rewards
    return out


def get_bandit_configs(graph_oracle, graph_sample, deg_r=0):
    configs = []
    lin_ucb_policy = LinUCBPolicy(1, log_normal=False)
    lin_ucb_bandit = MetricBandit(4, executer=NetworkExploringExecuter(graph_sample, graph_oracle,
                                                                       degree_threshold=deg_r))
    lin_ucb_agent = LinRelAgent(lin_ucb_bandit, lin_ucb_policy, log_normal=False)

    lin_policy = LinUCBPolicy(0, log_normal=False)
    lin_bandit = MetricBandit(4, executer=NetworkExploringExecuter(graph_sample, graph_oracle,
                                                                   degree_threshold=deg_r))
    lin_agent = LinRelAgent(lin_bandit, lin_policy, log_normal=False)

    poiss_ucb_policy = LinUCBPolicy(1, log_normal=True)
    poiss_ucb_bandit = MetricBandit(4, executer=NetworkExploringExecuter(graph_sample, graph_oracle,
                                                                         degree_threshold=deg_r))
    poiss_ucb_agent = LinRelAgent(poiss_ucb_bandit, poiss_ucb_policy, log_normal=True)

    # KNN Epsilon-Greedy
    knn_eps_greedy_bandit = MetricBandit(4, executer=NetworkExploringExecuter(graph_sample, graph_oracle,
                                                                              degree_threshold=deg_r))
    # knn_model = Pipeline([('scaler', StandardScaler()), ('knn', KNeighborsRegressor(n_neighbors=5,
    #                                                                                 weights='distance'))])
    knn_model = NormalizedKNeighborsRegressor(n_neighbors=20, weights='distance')
    knn_eps_greedy_agent = MetricAgent(knn_eps_greedy_bandit, EpsilonGreedyPolicy(0.1, n0=50),
                                       model=knn_model)

    knn_greedy_bandit = MetricBandit(4, executer=NetworkExploringExecuter(graph_sample, graph_oracle,
                                                                          degree_threshold=deg_r))
    knn_greedy_agent = MetricAgent(knn_greedy_bandit, GreedyPolicy(), model=knn_model)

    knn_ucb_bandit = MetricBandit(4, executer=NetworkExploringExecuter(graph_sample, graph_oracle,
                                                                       degree_threshold=deg_r))
    knn_ucb_agent = MetricAgent(knn_ucb_bandit, KNNUCBPolicy(1), model=knn_model)
    # bandit_configs.append(BanditConfig(linear_bandit, linear_agent, num_experiments=1))
    # bandit_configs.append(BanditConfig(linear_epsilon_greedy_bandit, Agent(linear_epsilon_greedy_bandit,
    #                                                            ucb_policy), num_experiments=5))
    # bandit_configs.append(BanditConfig(ucb_linear_bandit, Agent(ucb_linear_bandit,
    #                                                                    ucb_policy), num_experiments=1))
    # bandit_configs.append(BanditConfig(ucb_linear_clust_bandit, Agent(ucb_linear_clust_bandit,
    #                                                                    ucb_policy), num_experiments=5))
    configs.append(BanditConfig(knn_ucb_bandit, knn_ucb_agent, num_experiments=1))
    configs.append(BanditConfig(knn_greedy_bandit, knn_greedy_agent, num_experiments=1))
    configs.append(BanditConfig(knn_eps_greedy_bandit, knn_eps_greedy_agent, num_experiments=5))
    #
    # configs.append(BanditConfig(lin_bandit, lin_agent, num_experiments=1))
    configs.append(BanditConfig(lin_ucb_bandit, lin_ucb_agent, num_experiments=1))
    # bandit_configs.append(BanditConfig(poiss_ucb_bandit, poiss_ucb_agent, num_experiments=1))
    return configs


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Bandit based graph explorer')
    parser.add_argument('file', help='the file which needs to be sampled')
    parser.add_argument('-s', '--sample-size', type=float, default=0.05,
                        help='fraction of nodes to be used as the initial sample')
    parser.add_argument('-b', '--budget', type=int, default=1000)
    parser.add_argument('-e', '--experiments', type=int, default=10, help="Number of experiments to be done."
                                                                          "A new random sample is "
                                                                          "created each time")
    parser.add_argument('-m', '--sampling_method', default='rw',
                        choices=['rw', 'bfs', 'rn'], help="How the original graph should be sampled."
                                                          "Available methods: Random node (RN), "
                                                          "Random Walk (RW), Breadth first search (BFS)")
    parser.add_argument('--alpha', default=2, help="The exploration "
                                                   "coefficient (alpha) of iKNN-UCB algorithm")
    parser.add_argument('-plot', '--results_dir', default='./', help="Path to where the reward plot"
                                                                     " should be saved to")

    args = parser.parse_args()
    graph_file = args.file
    print("opening graph " + graph_file)
    graph_name = os.path.basename(graph_file).split('.')[0]
    graph_ = nx.read_edgelist(graph_file)
    print("size of the original graph: nodes=%d edges=%d"
          % (graph_.number_of_nodes(), graph_.number_of_edges()))
    graph_oracle = GraphOracle(graph_)
    graph_sample = None
    sample_size = args.sample_size
    if sample_size < 1.:
        sample_size = sample_size * graph_.number_of_nodes()
    num_initializations = args.experiments
    budget = args.budget

    results = []
    heuristic_policies = ['mod', 'random']
    heuristic_rounds = [1, 5]
    deg_r = 10
    # heuristic_policies = ['oracle', 'mod', 'random', 'mod_random, ]

    for sample_idx in range(num_initializations):
        print("probing sample: %d" % (sample_idx + 1))
        if args.sampling_method == 'rw':
            graph_sample = graph_oracle.get_rw_subgraph(sample_size)
        elif args.sampling_method == 'rn':
            graph_sample = graph_oracle.get_random_node_sample(sample_size)
        elif args.sampling_method == 'bfs':
            graph_sample = graph_oracle.get_bfs_subgraph(sample_size)
        g0 = graph_sample.copy()
        bandit_configs = get_bandit_configs(graph_oracle, graph_sample, deg_r=deg_r)

        print("initial %s sample: nodes=%d edges=%d avg clustering=%.2f"
              % (args.sampling_method, graph_sample.number_of_nodes(), graph_sample.number_of_edges(),
                 nx.average_clustering(graph_sample)))

        for policy, times in zip(heuristic_policies, heuristic_rounds):
            result = run_experiments(graph_oracle, graph_sample, budget=budget,
                                     sampling_func=mod_sample, experiments=times,
                                     plot=False, policy=policy, degree_threshold=deg_r)
            results.append(result)

        for bandit_config in bandit_configs:
            results.append(bandit_sample(bandit_config, budget, plot=False))

    results_map = defaultdict(list)
    print("number of nodes in each sample")
    for result in results:
        results_map[result.label].append(result.scores)

    sns.set(style='white', font_scale=1.1)
    pal = sns.color_palette(sns.diverging_palette(255, 133, l=60, n=len(results), center="dark"))
    plt_handles = []
    line_styles = ['-', '--', '-.', ':']
    line_cycler = cycle(line_styles)
    plt.title("new nodes found in each iteration")
    for idx, label in enumerate(results_map.keys()):
        avg_scores = np.average(results_map[label], axis=0)
        print("%s : %s" % (label, np.average(np.sum(avg_scores))))
        handle, = plt.plot(np.cumsum(avg_scores), label=label, linewidth=2.75,
                           color=pal[idx], linestyle=next(line_cycler))
        plt_handles.append(handle)

    plt.xlabel('number of probes')
    plt.ylabel('number of observed nodes')
    plt.legend(plt_handles, list(results_map.keys()), loc=4, prop={'size': 14})
    plot_file = '%s_%s_plot.pdf' % (graph_name, args.sampling_method)
    plt.savefig(os.path.join(args.results_dir, plot_file))
    plt.show()

    pickle_file = '%s_%s_results.pkl' % (graph_name, args.sampling_method)
    file = open(os.path.join(args.results_dir, pickle_file), 'wb')
    pickle.dump(results, file=file)
