# -*- coding: utf-8 -*-
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle
import matplotlib
import seaborn as sns
import pandas as pd
import os.path as path
import pickle as pkl
from collections import defaultdict
from mab_explorer.graph_oracle import GraphOracle

from networkx.algorithms import community


class GraphSample(object):
    def __init__(self, num_nodes, num_edges, optimal=None, scores=None, label=None):
        self.scores = scores
        self.num_nodes = num_nodes
        self.num_edges = num_edges
        self.optimal = optimal
        self.label = label
        self.rewards = None

def plot_deg_dist(graph):
    print("not implemented")

markers_array = list(matplotlib.markers.MarkerStyle.markers.keys())
line_styles = ["-","--","-.",":"]
line_cycler = cycle(line_styles)


def plot_test():
    mu = [0.1, 0.2, 0.3, 0.4, 0.5]
    #x = [0, 1, 2, 3, 4, 5, 6, 6.64385619, 7, 7.285402219, 7.64385619, 8]
    x = [1, 2, 4, 8, 16, 32, 64, 100, 128, 156, 200, 256]
    results = dict()
    # results['Oracle'] = [32665, 33009, 33441, 33243, 33798]
    # results['Random'] = [20034, 20548, 21274, 21510, 21029]
    # results['MOD'] = [21025, 22924, 24167, 28585, 31001]
    # results['Bandit'] = [28012, 28081, 29414, 29448, 30335]
    results['Bandit'] = [20641, 20533, 20822, 22022, 22290, 23259, 23596, 23766, 23883, 23675, 23725, 23711]

    sns.set(style='white', font_scale=1)
    pal = sns.color_palette(sns.diverging_palette(255, 133, l=60, n=len(mu), center="dark"))

    plt_handles = []
    plt.title("new nodes found in each iteration")

    for idx, item in enumerate(results.items()):
        handle, = plt.plot(x, item[1], label=item[0], linewidth=2.5,
                           linestyle=next(line_cycler), color=pal[idx])
        # labels.append(result.label)
        plt_handles.append(handle)

    plt.xscale('log', basex=2)
    plt.xlabel("number of clusters (log scale)")
    plt.ylabel("number ")
    # plt.legend(plt_handles, results.keys(), loc=4)
    plt.show()


def plot_lfr(file):
    results = pd.read_csv(file, sep='\t')
    plot_cols = ['Random', 'MOD', 'Lin-UCB', 'KNN-greedy', 'KNN-eps-greedy', 'iKNN-UCB']

    sns.set(style='white', font_scale=1)
    pal = sns.color_palette(sns.diverging_palette(255, 133, l=60, n=len(plot_cols), center="dark"))
    pal[-1] = (1., 0., 0.)
    plt_handles = []
    plt.title("new nodes found in each iteration")

    for idx, col in enumerate(plot_cols):
        handle, = plt.plot(results['mu'], results[col], label=col, linewidth=2.5,
                           linestyle=next(line_cycler), color=pal[idx])
        plt_handles.append(handle)

    plt.ylabel("number of nodes")
    plt.xlabel("mixing parameter")
    plt.legend(plt_handles, plot_cols, loc=7, prop={'size':12})
    plt.savefig(path.basename(file).split('.')[0] + '.pdf')
    plt.show()


def factor_bar_plot(file):
    results = pd.read_csv(file, sep=',')
    plot_cols = ['Random', 'MOD', 'Lin-UCB', 'iKNN-UCB']
    flattened = results.melt(id_vars=['Graph'],
                             value_vars=plot_cols, var_name='method',
                             value_name='nodes')

    sns.set(style='white', font_scale=1.06)
    pal = sns.color_palette(sns.diverging_palette(255, 133, l=60, n=len(plot_cols),
                                                  center="dark"))
    pal[-1] = (1., 0., 0.)
    factor_plot = sns.factorplot(x='method', y='nodes', col='Graph', kind='bar',
                                 aspect=0.5, data=flattened, palette=pal)
    factor_plot.set_xticklabels(rotation=90)
    plot_file = 'results/{}_bar.pdf'.format(path.basename(file).split('.')[0])
    factor_plot.savefig(plot_file)
    plt.show()


def plot_results(file):
    graph_name = 'BA'
    sampling_method = 'bfs'
    results_map = defaultdict(list)
    results = pkl.load(open(file, 'rb'))
    for result in results:
        results_map[result.label].append(result.scores)

    sns.set(style='white', font_scale=1)
    pal = sns.color_palette(sns.diverging_palette(255, 133, l=60, n=len(results), center="dark"))
    plt_handles = []
    line_styles = ['-', '--', '-.', ':']
    line_cycler = cycle(line_styles)
    plt.title("new nodes found in each iteration")

    labels = ['random', 'mod', 'LIN-UCB (α = 1.0)', 'KNN(n=20)-greedy',
              'KNN(n=20)-ε-greedy (ε=0.1)', 'KNN(n=20)-UCB (α = 2.0)']
    plot_label = ['random', 'mod', 'LIN-UCB', 'KNN-greedy',
              'KNN-ε-greedy', 'iKNN-UCB']
    pal[len(labels)-1] = (1., 0., 0.)
    for idx, label in enumerate(labels):
        # col = plt.cm.inferno((idx+1)/
        avg_scores = np.average(results_map[label], axis=0)
        print("%s : %s" % (label, np.average(np.sum(avg_scores))))
        print(plot_label[idx])
        handle, = plt.plot(np.cumsum(avg_scores), label=plot_label[idx], linewidth=2.5,
                           color=pal[idx], linestyle=next(line_cycler))
        #plt.fill_between()
        plt_handles.append(handle)

    plt.xlabel('number of probes')
    plt.ylabel('number of observed nodes')
    plt.legend(plt_handles, list(plot_label), loc=4)
    plot_file = '%s_%s_plot.pdf' % (graph_name, sampling_method)
    plt.savefig(path.join('results', plot_file))
    plt.show()

if __name__ == '__main__':
    data_file = '/Users/kaumad/Google Drive/Tokyotech/PhD/' \
                'Research/Results/output/BA_50k_20_bfs_results.pkl'
    plot_results(data_file)