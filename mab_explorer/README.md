### Dependencies:
* networkx
* bandits
* scikit-learn

### Data
Network datasets are downloaded from [Stanford Large Network Data collection](http://snap.stanford.edu/data/index.html).
Two example edgelist files are included in 'data' directory.

## Basic Usage
1. Supports Python 3.x only.
1. [Bandits](https://bitbucket.org/kau_mad/bandits/src/pkdd2018/) library should be installed first. Please refer the relevant Readme file for more details.

### Options
You can find all the available arguments to the probing algorithm by running

``python mab_explorer/sampling.py --help
``

If this throws a "module not found error". Add the current directory to PYTHONPATH by executing
``export PYTHONPATH=.``.


### Input
The network data should be provided as an edgelist file. Two sample files are provided in ``[project home]/data`` directory.

### Example
Executing the following command runs *i*KNN-UCB and all other baseline probing algorithms on Astro Physics collaboration network.

``python mab_explorer/sampling.py data/CA-AstroPh.txt
 -s 0.05 -b 1000 -m rn -e 1 -plot ./results``
 
### Output
End of running all probing algorithms, it prints the number of additional nodes found by each probing algorithm.
As an example:

    number of nodes in each sample '
    mod : 8080.0
    random : 8316.6
    KNN(n=20)-ε-greedy (ε=0.1) : 8840.2
    LIN-UCB (α = 1.0) : 8735.0
    KNN(n=20)-UCB (α = 2.0) : 8756.0
    KNN(n=20)-greedy : 8820.0
    
The cumulative number of additional nodes discovered in each probing step will be displayed in a plot.
Example:    
![Rewards](results/ca-astroph-bfs.png)